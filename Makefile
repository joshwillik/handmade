CC = gcc
CFLAGS = -Wall -Wextra -Werror -LSDL `sdl2-config --cflags --libs`

bin/handmade: main.o
	mkdir -p bin
	$(CC) $(CFLAGS) main.o -o bin/handmade
