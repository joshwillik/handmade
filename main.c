#include <SDL.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#define local_persist static
#define global_variable static
#define internal_function static
#define PATH_LENGTH 50
#define GAME_WIDTH 1366
#define GAME_HEIGHT 768
#define pixel_t uint32_t
#define BLACK 0
#define WHITE 255<<16 | 255<<8 | 255
#define GREEN 0<<16 | 255<<8 | 0

typedef struct {
	int x;
	int y;
} point;

typedef struct {
	point from;
	point to;
} line;

void clear_surface(SDL_Surface *screen, pixel_t color) {
	SDL_FillRect(screen, &(SDL_Rect){.x=0, .y=0, .w=screen->w, .h=screen->h}, color);
}

void set_pixel(SDL_Surface *screen, point coords, pixel_t color) {
	pixel_t *pixels = (pixel_t*)screen->pixels;
	pixels[coords.y * screen->pitch / sizeof(coords.y) + coords.x] = color;
}

void draw_line(SDL_Surface *screen, point from, point to, pixel_t color) {
	for (float t = 0.; t < 1.; t+=0.001) {
		int x = from.x + (to.x-from.x)*t;
		int y = from.y + (to.y-from.y)*t;
		set_pixel(screen, (point){x, y}, color);
	}
}

bool render_model(SDL_Window *game_window, line *path, size_t num_lines, pixel_t color) {
	SDL_Surface *screen = SDL_GetWindowSurface(game_window);
	if (screen == NULL) {
		SDL_Log("Unable to init SDL screen: %s", SDL_GetError());
		return false;
	}
	SDL_LockSurface(screen);
	clear_surface(screen, BLACK);
	for (size_t i = 0; i < num_lines; i++) {
		draw_line(screen, path[i].from, path[i].to, color);
	}
	SDL_UnlockSurface(screen);
	SDL_UpdateWindowSurface(game_window);
	return true;
}

bool should_quit() {
	SDL_Event e;
	while (SDL_PollEvent(&e)) {
		switch (e.type) {
			case SDL_QUIT:
			return true;
			break;
		}
	}
	return false;
}

point random_point(SDL_Window *window) {
	SDL_Surface *screen = SDL_GetWindowSurface(window);
	int x = rand() % screen->w;
	int y = rand() % screen->h;
	return (point){x, y};
}

int main() {
	srand(time(NULL));
	if (SDL_Init(SDL_INIT_VIDEO)) {
		SDL_Log("Unable to init SDL: %s", SDL_GetError());
		return 1;
	}
	SDL_Window *game_window =
		SDL_CreateWindow("Handmade", 100, 100, GAME_WIDTH, GAME_HEIGHT, SDL_WINDOW_SHOWN);
	if (game_window == NULL) {
		SDL_Log("Unable to init SDL window: %s", SDL_GetError());
		goto exit;
	}
	size_t num_lines = 0;
	line* path = calloc(sizeof(line)*PATH_LENGTH, 0);
	while (!should_quit()) {
		if (!render_model(game_window, path, num_lines, GREEN)) {
			SDL_Log("Unable to render_model frame");
			goto exit;
		}
		if (num_lines==PATH_LENGTH) {
			for (size_t i = 0; i < PATH_LENGTH-1; i++) {
				path[i] = path[i+1];
			}
			num_lines--;
		}
		line next;
		if (num_lines) {
			next = (line){path[num_lines-1].to, random_point(game_window)};
		} else {
			next = (line){random_point(game_window), random_point(game_window)};
		}
		path[num_lines++] = next;
		SDL_Delay(1000/60);
	}
	printf("Quitting...");
	exit:
	return 0;
}
